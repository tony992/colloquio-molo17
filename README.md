Installazione e uso
verisione di node 17.4.0 versione npm 8.3.1

documentazione api
https://molo17.stoplight.io/docs/molo-17/YXBpOjQwMTc1MDQ4-table-soccer

aprire un terminale
cd server
creare il file .env
PORT = 3001 // porta del server
MONGO_URI = "mongodb://localhost:27017/molo17" // connessione a mongodb locale
JWT_SECRET = ":m7DNL3r==-<&xV9p?6CNdy" // secret per jwt
SOCKET = 3002 // porta della socket
ORIGIN = "http://localhost:3000" // url del client


eseguire i seguenti comandi:
npm install // per installare tutte le dipendenza
npm run build // per creare la build
npm run start // per eseguire il server

aprire un altro termianale
cd client
eseguire i seguenti comandi:
npm install
npm run build
serve -s build // per eseguire la build in locale

il client non ho avuto tempo di finirlo quindi ci saranno delle procedure da eseguire mediante postman ma i risultati si possono vedere lato client.
registrazione utente (no giocatore) tramite signup lato client login utente mediante sign-in lato client, il login restituirà della durata di un ora che ci servirà per le chiamate da fare con postman (il token è possibile reperirlo nei cookie o nella risposta).
eseguire i seguenti step mediante postman, consultare la documentazione delle api per l'esecuzione:
tutte le chiamate api dovranno essere effettuate con l'header Authorization: Bearer token
Creazione di 4 player
Creazione di 2 team
Creazione di 1 match (a questo punto il match verrà automaticamente aggiunto sulla lista lato client)
eseguire l'aggiornamento dello score definendo queale team segna (la modifica comporterà un aggiornamento lato client), al raggiungimento del valore 8 di uno dei due team la partita termina e lo status lato client passerà da outgoing a ended.
eseguire la get dei player, basterà non inserire nessun parametro sulla query, lì si vedranno le statistiche aggiornate dei giocatori.
