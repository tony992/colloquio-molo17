import express from 'express';
import { Request, Response } from 'express'

import { connect} from 'mongoose';

import session from 'express-session';
import cookieParser from 'cookie-parser';
import { exit,env } from 'process';
import UserModel,{ IUser} from './models/User';
import * as dotenv from "dotenv";
import passport from 'passport'
import cors from 'cors';
import responseTime from 'response-time';
import {local, Jwt} from './middleware/passport';
import authRouter from './routes/auth/handler';
import teamRouter from './routes/team/handler';
import playerRouter from './routes/player/handler';
import matchRouter from './routes/match/handler';
import {Server} from 'socket.io';
import { ClientToServerEvents, InterServerEvents, ServerToClientEvents, SocketData } from './interface/socket/socketInterface';
dotenv.config();
const port  = env.PORT;
const mongoUri = env.MONGO_URI;
const socketPort = Number(env.SOCKET);
const client = env.ORIGIN||"http://localhost:3000";
const app = express();
app.use(responseTime((req:Request, res:Response, time) => {
  console.log(`${res.statusCode} ${req.method} ${req.originalUrl} ${time.toFixed(3)}ms`);
}))
app.use(cors({
  origin: [client],
  credentials:true
}));
const io = new Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>(socketPort,{
  cors: {
    origin: '*',
  }});
app.use(express.json());

app.use(
  session({
    secret: "secretcode",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
passport.use('local',local)
passport.use('jwt',Jwt)

type User = {
  _id?: number
}
passport.serializeUser((user:User, cb) => {
  cb(null, user._id);
});

passport.deserializeUser((id: string, cb) => {
  UserModel.findOne({ _id: id }, (err:Error, user: IUser) => {
    const userInformation = user.userPrivate();
    cb(err, userInformation);
  });
});

connect(`${mongoUri}`)
.then(()=> {
  app.listen(port, ()=> {
    console.log(`Application is listening on port:${port}`)
  })
})
.catch(err => {
  console.log(err)
  exit(1);
});

app.use('/auth',authRouter);
app.use('/team',teamRouter);
app.use('/match',matchRouter);

app.use('/player',playerRouter);


export {io};