
import { Request, Response } from 'express'
import UpdateResponse from '../../interface/db/UpdateResponse';
import TeamsModel from '../../models/Team';


function updateTeam( req: Request, res: Response): Promise<Response> {
    const update = req.body;
    console.log(update)
    return TeamsModel.updateOne({_id: req.body._id}, {$set: update})
    .then((optMatch) => {
        const opt =  <UpdateResponse>optMatch;
        console.log(opt);
        if(opt.modifiedCount ===1){
            return res.status(200).json({ message:"match modified"  });
        } 
        else if( opt.matchedCount===0 ){
            return  res.status(404).json({message: 'match not found'})
        } 
        else {
            return  res.status(400).json({message: 'not modified'})
        }
    })
    .catch((err: Error)=> {
        console.log(err)
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default updateTeam