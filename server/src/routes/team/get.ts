
import { Request, Response } from 'express'
import TeamsModel, { ITeam } from '../../models/Team';
import Joi from 'joi';


function getTeams(req: Request, res: Response): Promise<Response> {
    return Promise.resolve()
    .then(()=> {
        const QuerySchema = Joi.object({
            timestamp: Joi.number().required(),
            name: Joi.string(),
            striker: Joi.string(),
            defensor: Joi.string()
        });
        const { error } = QuerySchema.validate(req.query);
        if(error){
            throw error
        }
    })
    .then(()=> {
        const filter = req.query||{};
        return TeamsModel.find(filter)
        .populate('striker',["username"])
        .populate('defensor',['username'])
        .exec()
    })
    .then((teams:Array<ITeam>) => {
        return res.status(200).json({ teams, timestamp:req.query.timestamp });
    })
    .catch((err: Error)=> {
        console.log(err)
        if(err.name === "ValidationError"){
            return res.status(400).send(err.stack)
        }
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default getTeams
