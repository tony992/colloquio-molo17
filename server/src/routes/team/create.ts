
import { Request, Response } from 'express'
import TeamsModel, { ITeam } from '../../models/Team';
import Joi from 'joi';
import PlayerModel from '../../models/Player';

function createTeam( req: Request, res: Response): Promise<Response> {
    return Promise.resolve()
    .then(()=> {
        const bodySchema = Joi.object({
            name: Joi.string().required(),
            striker: Joi.string().required(),
            defensor: Joi.string().required().disallow(Joi.ref('striker'))
        });
        const { error } = bodySchema.validate(req.body);
        if(error){
            throw error
        }
        return PlayerModel.find({
            _id: {
                $in:[req.body.striker,req.body.defensor]
            }
        })
        .select({_id:1})
        .then((players)=>{
            if(players.length!==2){
                throw new Error("Not Found");
            }
            return true
        })
    })
    .then(()=> {
        const team:ITeam = new TeamsModel(req.body);
        return team.save()
    })
    .then((newTeam) => {
        return res.status(200).json({ team:newTeam  });
    })
    .catch((err: Error)=> {
        console.log(err.name, err.message, err.stack);
        if(err.name === "ValidationError"){
            return res.status(400).send(err.stack)
        } else if(err.message === "Not Found"){
            return res.status(404).send("Players not found");
        }
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default createTeam
