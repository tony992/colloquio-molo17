
import { Request, Response } from 'express'
import MatchModel, { IMatch }from '../../models/Match';
import { io } from '../../server';
import Joi from 'joi';
import TeamsModel from '../../models/Team';
// import MongooseError from '../../interface/db/Error';


function createMatch( req: Request, res: Response): Promise<Response> {
    return Promise.resolve()
    .then(()=> {
        const bodySchema = Joi.object({
            status: Joi.string().allow("outgoing","ended").required(),
            redTeam: Joi.string().required(),
            blueTeam: Joi.string().required().disallow(Joi.ref('redTeam')),
            date: Joi.date()
        });
        const { error } = bodySchema.validate(req.body);
        if(error){
            throw error
        }
        return TeamsModel.find({
            _id: {
                $in:[req.body.redTeam,req.body.blueTeam]
            }
        })
        .select({_id:1})
        .then((teams)=>{
            if(teams.length!==2){
                throw new Error("Not Found");
            }
            return true
        })
    })
    .then(()=> {
        const match:IMatch = new MatchModel(req.body);
        return match.save()
    })
    .then((newMatch)=>
        MatchModel.findById(newMatch._id)
        .populate({
            path:'redTeam', 
            populate:[
                {
                    path: "striker"
                },
                {
                    path: "defensor"
                }
            ]
        })
        .populate({
            path:'blueTeam', 
            populate:[
                {
                    path: "striker"
                },
                {
                    path: "defensor"
                }
            ]
        })
        .exec()
    )
    .then((newMatch) => {
        const match:IMatch | null = newMatch 
        if(match){
          io.emit("addMatch", match)
        }       
        return res.status(200).json({ match });
    })
    .catch((err: Error)=> {
        if(err.name === "ValidationError"){
            return res.status(400).send(err.message)
        } else if(err.message === "Not Found"){
            return res.status(404).send("Teams not found");
        }
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default createMatch