
import { Request, Response } from 'express'
import MatchModel from '../../models/Match';
import Joi from 'joi';


function getMatches( req: Request, res: Response): Promise<Response> {
    return Promise.resolve()
    .then(()=> {
        const QuerySchema = Joi.object({
            status: Joi.string().valid("outgoing", "ended"),
            redTeam: Joi.string(),
            blueTeam: Joi.string(),
            date: Joi.date()
        });
        const { error } = QuerySchema.validate(req.query);
        if(error){
            throw error
        }
        return  true;
    })
    .then(()=> {
        const filter = req.query;
        return MatchModel.find(filter)
        .populate({
            path:'redTeam', 
            populate:[
                {
                    path: "striker"
                },
                {
                    path: "defensor"
                }
            ]
        })
        .populate({
            path:'blueTeam', 
            populate:[
                {
                    path: "striker"
                },
                {
                    path: "defensor"
                }
            ]
        })
        .exec()
    })
    .then((matches) => {
        return res.status(200).json({ matches });
    })
    .catch((err: Error)=> {
        console.log(err)
        if(err.name === "ValidationError"){
            return res.status(400).send(err.stack)
        }
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default getMatches