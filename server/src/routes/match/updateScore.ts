
import { Request, Response } from 'express'
// import { Query } from 'mongoose';
import { IUpdateMatch, IUpdateStat } from '../../interface/db/UpdateInterface';
import UpdateResponse from '../../interface/db/UpdateResponse';
import MatchModel ,{ IMatch }from '../../models/Match';
import PlayerModel from '../../models/Player';
import  { ITeam } from '../../models/Team';
import { io } from '../../server';
import Joi from 'joi';
const SCORE_TO_END = 7;

async function handleEndMatch (match:IMatch): Promise<void> {
    const property = ["redScore", "blueScore"];
    const winner = property.find(p => match.get(p) === SCORE_TO_END);
    const teamLabel = winner?.replace("Score","");
    const redTeam:ITeam = <ITeam> match.redTeam;
    const blueTeam:ITeam = <ITeam> match.blueTeam;

    const parsedTeam = [
        {team:"red", role: "striker", _id: redTeam.striker},
        {team:"red", role: "defensor", _id: redTeam.defensor},
        {team:"blue", role: "defensor", _id: blueTeam.defensor},
        {team:"blue", role: "striker", _id: blueTeam.striker}
    ]
    
    return parsedTeam.forEach(async(player) => {
        const update: IUpdateStat = {
            $inc: {
                total: 1
            }
        };
        if (teamLabel === player.team) {
            update.$inc.win = 1;
            if(player.role === "striker"){
                update.$inc[`strikerStatistics.win`] = 1;
            } else {
                update.$inc['defensorStatistics.win'] = 1;
                
            }
        } else {
            update.$inc.lose = 1;
            if(player.role === "striker"){
                update.$inc[`strikerStatistics.lose`] = 1;
            } else {
                update.$inc['defensorStatistics.lose'] = 1;
            }
        }
        console.log(update);
        await PlayerModel.updateOne({ _id: player._id }, update);
    })
}

function createMatch( req: Request, res: Response): Promise<Response> {
    let ended = false;
    interface score {
        red: number,
        blue: number
    }
    const scores = <score>{
        red:0,
        blue:0
    }
    const update:IUpdateMatch = {};

    return Promise.resolve()
    .then(()=> {
        const bodySchema = Joi.object({
            _id: Joi.string().required(),
            team: Joi.string().valid("red","blue").required(),
            type: Joi.string().valid("goal").required(),
        });
        const { error } = bodySchema.validate(req.body);
        if(error){
            throw error;
        }
    })
    .then(()=> {
        switch (req.body.team) {
            case "blue":
                if(req.body.type === "goal"){
                    update.$inc={ blueScore: 1}
                }
                break;
        
            case "red":
                if(req.body.type === "goal"){

                    update.$inc={ redScore: 1}
            }

                break;
            
            default:
                break;
        }
    
        return MatchModel.findOne({_id: req.body._id, status: "outgoing"})
        .populate("redTeam")
        .populate("blueTeam")
        .exec()
    })
    .then((oldMatch:IMatch|null)=> {
        if(!oldMatch){
            throw new Error("Not Found")
        } 
        scores.red = oldMatch.redScore;
        scores.blue = oldMatch.blueScore;
           
        const team = `${req.body.team}Score`;
        
        if(req.body.team === "red"){
            scores.red = Number(oldMatch.get(team)) + 1
        } else {
            scores.blue = Number(oldMatch.get(team)) + 1
        }
        
        if(oldMatch.get(team) === SCORE_TO_END){
            ended = true;
            update.$set = {status: "ended"};
            return handleEndMatch(oldMatch)
            .then(()=> {
                return MatchModel.updateOne({_id: req.body._id}, update)
            });
        }
        return MatchModel.updateOne({_id: req.body._id}, update)
    })
    .then((optMatch) => {
        const opt : UpdateResponse= optMatch;
         if(opt.modifiedCount ===1){
            io.emit("changeScore", req.body._id,  scores.red, scores.blue)
            if(ended){
                io.emit("changeStatus", req.body._id, "ended")
                return res.status(200).json({ message:"match is ended"  });    
            }
            return res.status(200).json({ message:"score modified"  });
        } 
        return  res.status(400).json({message: 'not modified'})
    })
    .catch((err: Error)=> {
        if(err.name === "ValidationError"){
            return res.status(400).send(err.message)
        } else if(err.message === "Not Found"){
            return res.status(404).send("Match not found or already ended");
        }
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default createMatch