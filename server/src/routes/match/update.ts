
import { Request, Response } from 'express'
import UpdateResponse from '../../interface/db/UpdateResponse';
import MatchModel ,{ IMatch }from '../../models/Match';


function createMatch( req: Request, res: Response): Promise<Response> {
    const match:IMatch = <IMatch> req.body;
    return MatchModel.updateOne({_id: req.body._id}, {$set: match})
    .then((optMatch) => {
        const opt =  <UpdateResponse> optMatch;
        if(opt.modifiedCount ===1){
            return res.status(200).json({ message:"match modified"  });
        } 
        else if( opt.matchedCount===0 ){
            return  res.status(404).json({message: 'match not found'})
        } 
        else {
            return  res.status(400).json({message: 'not modified'})
        }
    })
    .catch((err: Error)=> {
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default createMatch