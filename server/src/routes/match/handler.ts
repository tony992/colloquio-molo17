import {Router} from 'express';
import passport from 'passport';
import get from './get';
import create from './create';
// import update from './update';
import updateScore from './updateScore';   
const router = Router();
    router.route('/')
    .get(get)
    .post(passport.authenticate("jwt"),create)
    .put(passport.authenticate("jwt"),updateScore)

    // router.put("/score", updateScore)
    
  export default router;