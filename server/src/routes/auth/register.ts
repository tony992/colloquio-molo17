
import { Request, Response } from 'express'
import UserModel,{ IUser } from '../../models/User';

function registerUser( req: Request, res: Response): Promise<Response> {
    console.log("register api called", {body: req.body})
    const { firstName, password, email, lastName } = req.body;

    const user:IUser = new UserModel({
        name: firstName,
        lastName,
        email: email,
        password,
    });
    return user.save()
        .then((newUser) => {
            return res.status(200).json({ user: newUser });
        })
        .catch((err) => {
            if (err.code === 11000) {

                return res.status(409).json({ message: "user already exists" });
            }
            else if (err?.errors?.email?.message) {
                return res.status(400).json({ message: "Please fill a valid email address" });
            }
            return res.status(500).json({ message: "unknown error" });
        });
}
export default registerUser