import {Router} from 'express';
import passport from 'passport';
import login from './login';
import register from "./register";
import get from "./get";


    const router = Router();
    router.post('/',  register);
    router.get('/',passport.authenticate("jwt"),  get);
    router.post('/login',passport.authenticate("local"), login)

  export default router;