
import { Request, Response } from 'express'
import  { IUser } from '../../models/User';


function getUser( req: Request, res: Response): Response<Response> {
    const user = <IUser> req.user;
    const userParsed = user.userPrivate();
    return res.status(200).json({user:userParsed})
    }

export default getUser