
import { Request, Response } from 'express'
import { IUser } from '../../models/User';
import jwt from 'jsonwebtoken';
import * as dotenv from 'dotenv';
import { env } from 'process';
dotenv.config();

const jwtSecret = `${env.JWT_SECRET}`;

function login( req: Request, res: Response): Response<Response> {
    console.log("login api called")

    const user = <IUser> req.user;
    const userParsed = user.userPrivate();
    const token = jwt.sign({ 
      exp: Math.floor(Date.now() / 1000) + (60 * 60),
      _id: userParsed._id
     },  jwtSecret)
     res.cookie('molo17', token,{secure: false});
     return res.status(200).json(Object.assign({},{user: {...userParsed, token}}))
    }

export default login