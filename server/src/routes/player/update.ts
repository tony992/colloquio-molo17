
import { Request, Response } from 'express'
import UpdateResponse from '../../interface/db/UpdateResponse';
import PlayerModel from '../../models/Player';


function updatePlayer( req: Request, res: Response): Response | Promise<Response> {
    const update = req.body;
    const role = ["striker", "defensor"] 
    if(!req.body._id || role.indexOf(req.body.defaultRole) === -1){
        return res.status(400).send("Bad Request")
    }
    console.log(update)
    return PlayerModel.updateOne({_id: req.body._id}, {$set: update})
    .then((optPlayer) => {
        const opt =  <UpdateResponse>optPlayer;
        console.log(opt);
        if(opt.modifiedCount ===1){
            return res.status(200).json({ message:"player modified"  });
        } 
        else if( opt.matchedCount===0 ){
            return  res.status(404).json({message: 'player not found'})
        } 
        else {
            return  res.status(400).json({message: 'not modified'})
        }
    })
    .catch((err: Error)=> {
        // console.log(JSON.stringify(err.name), err.message)
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default updatePlayer