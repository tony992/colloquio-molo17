
import { Request, Response } from 'express'
import PlayerModel, { IPlayer } from '../../models/Player';
import MongooseError from '../../interface/db/Error';
import Joi from 'joi';

function createPlayer( req: Request, res: Response): Promise<Response> {
    console.log("create player called", req.body)
    return Promise.resolve()
    .then(()=> {
        const bodySchema = Joi.object({
            username: Joi.string().required(),
            defaultRole: Joi.string().valid("striker","defensor").required(),
        });
        const { error } = bodySchema.validate(req.body);
        if(error){
            throw error
        }
    })
    .then(()=> {
        const player:IPlayer = new PlayerModel(req.body);
        return player.save()
    })
    .then((newPlayer) => {
        return res.status(200).json({ player:newPlayer  });
    })
    .catch((err: MongooseError)=> {
        if (err?.code === 11000) {
            return res.status(409).send("player already exists");
        }
        if(err?.name === "ValidationError"){
            return res.status(400).send(err.message)
        }
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default createPlayer