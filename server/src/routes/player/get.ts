
import { Request, Response } from 'express'
import PlayerModel from '../../models/Player';


function getTeams( req: Request, res: Response): Promise<Response> {
    interface QueryPayer {
        username?:RegExp
    }
    const filter:QueryPayer = {};
    if(req?.query?.name){
        const startWithRegex = `^${req?.query?.name}.*`;
         filter.username= new RegExp(startWithRegex, "i")
    }
    return PlayerModel.find(filter)
    .exec()
    .then((players) => {
        return res.status(200).json({ players, timestamp:req?.query?.timestamp });
    })
    .catch((err: Error)=> {
        return res.status(500).json({error: err, message: 'unkown error'})
    })
    }

export default getTeams