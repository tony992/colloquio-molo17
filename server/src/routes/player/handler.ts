import {Router} from 'express';
import passport from 'passport';
import get from './get';
import create from './create';
import update from './update';
    const router = Router();
    router.route('/')
    .get(get)
    .post(passport.authenticate("jwt"),create)
    .put(update)
    
  export default router;