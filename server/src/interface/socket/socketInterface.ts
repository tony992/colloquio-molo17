import { IMatch } from "../../models/Match";

export interface ServerToClientEvents {
    noArg: () => void;
    basicEmit: (a: number, b: string, c: Buffer) => void;
    withAck: (d: string, callback: (e: number) => void) => void;
    changeScore:(id:string, redScore: number, blueScore: number)=>void;
    changeStatus:(id:string, ended: string)=>void;
    addMatch: (match: IMatch)=> void;
  }
  
  export interface ClientToServerEvents {
    hello: () => void;
  }
  
  export interface InterServerEvents {
    ping: () => void;
  }
  
  export interface SocketData {
    name: string;
    age: number;
  }