interface MongooseError extends Error {
    code: number,
    index: number
}
export default MongooseError