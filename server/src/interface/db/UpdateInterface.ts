export interface IUpdateStat {
    $inc: {
        total:number,
        win?:number,
        lose?:number,
        ['strikerStatistics.win']?:number,
        ['strikerStatistics.lose']?:number,
        ['defensorStatistics.win']?:number,
        ['defensorStatistics.lose']?:number,
    }
}

export interface IUpdateMatch {
    $inc?: {
        blueScore?:number,
        redScore?:number
    },
    $set?:{
        status?: string
    }
}