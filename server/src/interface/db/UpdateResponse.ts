interface UpdateResponse {
    acknowledged: boolean,
    modifiedCount: number,
    upsertedId?: unknown,
    upsertedCount: number,
    matchedCount: number
   }

   export default UpdateResponse