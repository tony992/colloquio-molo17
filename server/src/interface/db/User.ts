

interface User {
    name: string;
    avatar?: string;
    _id?: string;
    email:string
}
export default User;
