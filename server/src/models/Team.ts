/* eslint-disable @typescript-eslint/no-this-alias */
import { Schema, model, Document, Types } from 'mongoose';

interface ITeam extends Document {
    name: string;
    striker: Types.ObjectId | Record<string, unknown>;
    defensor: Types.ObjectId | Record<string, unknown>;
    _id: string;
  }

const teamsSchema: Schema <ITeam> = new Schema({
    name: { type: String, required: true },
    striker: {
            type: Types.ObjectId,
            ref: "Player",
            required: true
    },
    defensor: {
        type: Types.ObjectId,
        ref: "Player",
        required: true
    },
});
  

    // teamsSchema.pre<ITeam>('updateOne', function(this:any, next){
        
    //     const score:number = this?.getUpdate()?.$set?.score;
    //     if(score && score > 8){

    //         throw new Error('numero troppo grande')
    //     }
    //     next()
        
    //   })

const TeamsModel = model<ITeam>('Team', teamsSchema);
export default TeamsModel;
export {ITeam}
