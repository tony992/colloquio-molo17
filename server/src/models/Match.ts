/* eslint-disable @typescript-eslint/no-this-alias */
import { Schema, model, Document, Types } from 'mongoose';
import { ITeam } from './Team';

interface IMatch extends Document {
    date: Date;
    redTeam: Types.ObjectId | ITeam;
    blueTeam: Types.ObjectId |ITeam;
    redScore: number,
    blueScore: number,
    _id: string;
    status: Enumerator<string>;
  }

const matchSchema: Schema <IMatch> = new Schema({
    date: { type: Date, required: true, default:Date.now },
    redTeam: {
            type: Types.ObjectId,
            ref: "Team",
            required: true
    },
    blueTeam: {
        type: Types.ObjectId,
        ref: "Team",
        required: true
    },
    redScore: {type:Number, max:8, default:0},
    blueScore: {type: Number, max:8, default: 0},
    status: {
        type: String,
        enum: ['outgoing','ended']
        }

    });
  

matchSchema.index({status:1})
    
const MatchModel = model<IMatch>('Match', matchSchema);
export default MatchModel;
export { IMatch}
