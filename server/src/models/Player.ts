import { Schema, model, Document } from 'mongoose';
enum role {
    striker,
    defensor
}
export interface IPlayer extends Document {
    username: string;
    total:number,
    win: number,
    lose: number,
    defaultRole: role | Record<string, unknown>,
    strikerStatistics:{
        win: number,
        lose: number
    },
    defensorStatistics: {
        win:number,
        lose: number
    },
  }

const playerSchema: Schema <IPlayer> = new Schema({
    username: { type: String, required: true, unique: true },
    total:{type:Number, default:0},
    win: {type:Number, default:0},
    lose: {type:Number, default:0},
    strikerStatistics:{
        win: {type:Number, default:0},
        lose: {type:Number, default:0}
    },
    defensorStatistics: {
        win:{type:Number, default:0},
        lose: {type:Number, default:0}
    },
    defaultRole:{
        type:String,
        enum: ["striker","defensor"]
    }
   });
  
playerSchema.index({name:1})
  
    
const PlayerModel = model<IPlayer>('Player', playerSchema);
export default PlayerModel;
