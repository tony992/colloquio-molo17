/* eslint-disable @typescript-eslint/no-this-alias */
import { Schema, model, Document } from 'mongoose';
import bcrypt from 'bcryptjs';
import UserPrivate from '../interface/db/User';

const regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export interface IUserDocument extends Document {
  name: string;
  email: string;
  avatar?: string;
  _id: string;
  password: string;
  lastName:string;
}
 export interface IUser extends IUserDocument {
    checkPassword: (password: string) => Promise<boolean>;
    userPrivate: () => IUserDocument ;
  }
  

const validateEmail = function(email:string) {
    const re = new RegExp(regexEmail)
    return re.test(email)
};
const userSchema: Schema <IUser> = new Schema({
    name: { type: String, required: true },
    lastName:{ type: String, required: true },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        required: true,
        validate: [validateEmail, 'Please fill a valid email address'],
        match: [regexEmail, 'Please fill a valid email address']
    },
    password: {type: String, required:true},
    avatar: String,
    });
  

    userSchema.pre<IUser>('save', function save(next) {
        
        const user = this
      
        bcrypt.genSalt(10, (err, salt) => {
          if (err) {
            return next(err)
          }
          bcrypt.hash(this.password, salt, (err: Error, hash:string) => {
            if (err) {
              return next(err)
            }
            user.password = hash
            next()
          })
        })
      })

    userSchema.methods.checkPassword = async function (password: string) {
        const result = await bcrypt.compare(password, this.password);
        return result;
    };
    userSchema.methods.userPrivate = function (this:IUser) {
        const user: UserPrivate = {
            email:this.email,
            name: this.name,
            avatar: this.avatar,
            _id:this._id
        }
        return user;
    };
    userSchema.index({email:1})
const UserModel = model<IUser>('User', userSchema);
export default UserModel;
