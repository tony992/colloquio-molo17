import passportJwt from 'passport-jwt'
import passportLocal from 'passport-local';
import User from '../models/User';
import { IUser} from '../models/User';
import { env } from 'process';
import * as dotenv from 'dotenv';
dotenv.config()
const JwtStrategy = passportJwt.Strategy
const ExtractJwt = passportJwt.ExtractJwt
const jwtSecret = `${env.JWT_SECRET}`;
const LocalStrategy = passportLocal.Strategy

const local = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
},(email: string, password: string, done) => {
    User.findOne({ email: email }, (err:Error, user: IUser) => {
      if (err) throw err;
      if (!user) return done(null, false);
     return user.checkPassword(password)
      .then((isMatch:boolean)=> {
          if(isMatch){
            return done(undefined, user)  
          } else {
            return done(undefined, false, { message: 'Invalid username or password.' })
          }
      })
      .catch((error:Error)=> {
        return done(error)
     })
    });
  })
  

  const Jwt = new JwtStrategy(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: jwtSecret,
      },
      function (jwtToken, done) {
        User.findOne({ _id: jwtToken._id }, function (err:Error, user:IUser) {
          if (err) {
            return done(err, false)
          }
          if (user) {
            return done(undefined, user, jwtToken)
          } else {
            return done(undefined, false)
          }
        })
      }
    )
  
  export {local,Jwt}