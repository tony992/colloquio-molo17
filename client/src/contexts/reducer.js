
  export const reducer = (action, state) => {
    switch (action.type) {
      case "UPDATE_MATCH":
        return { ...state, matches: action.payload };
      case "UPDATE_USER":
        return { ...state, user: action.payload };
      default:
        return state;
    }
  };
  
  // export default function mapDispatchToProps(dispatch) {
  //   return {
  //     updateMatch: payload =>
  //       dispatch({ type: "UPDATE_MATCH", payload }),
  //     updateUser: payload =>
  //       dispatch({ type: "UPDATE_USER", payload }),
  //     updateAlert: payload => dispatch({ type: "UPDATE_ALERT", payload })
  //   };
  // }
  