import React, {createContext,useReducer} from 'react';
import { reducer } from './reducer';
import { SocketProvider } from './socketContext';


const init = {
    matches: [],
    user:{}
}
export const AppContext = createContext();
export const AppProvider = (props) => {
    const [state, dispatch] = useReducer(
        (state, action) => reducer(action, state),init);
    return (
    <SocketProvider>
      <AppContext.Provider value={{ dispatch,state}}>
        {props.children}
      </AppContext.Provider>
    </SocketProvider>
    );
  }