import  {createContext} from 'react';
import config from '../config.json';
const { io } = require("socket.io-client");

export const socket = io.connect(config.socketUrl,{
    transports: ['websocket'], 
    upgrade: false
});
export const SocketContext = createContext({socket});

export const SocketProvider = (props) => {
    return (
    <SocketContext.Provider value={socket}>
        {props.children}
    </SocketContext.Provider>
    );
  }
