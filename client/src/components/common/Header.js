import React, { useContext, useEffect, useState } from "react";
import {Link} from "react-router-dom";
import { AppContext } from "../../contexts/appContext";
import { Icon } from '@iconify/react';
import Cookies from 'js-cookie';
import { getUser } from "../../api/autApi";

const NotAuth = ()=> {
return (<div className="navbar-collapse" style={{"justifyContent": "end"}} id="navbarTogglerDemo02">
<ul className="navbar-nav ml-auto">
  <li className="nav-item">
    <Link className="nav-link" to={"/sign-in"}>Login</Link>
  </li>
  <li className="nav-item">
    <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
  </li>
</ul>
</div>)
}
const Header = ()=> {
  const {dispatch, state} = useContext(AppContext);
  const[ auth,setAuth ]= useState(false);  
  useEffect(()=> {
    if(state.user && state.user.email){
      setAuth(true);
      } else {

        setAuth(false)
      } 
      
  }, [state.user])
  useEffect(()=> {
    const token = Cookies.get("molo17");
    if(token){
      getUser(token).then((response)=> {
        console.log(response)
        dispatch({type:"UPDATE_USER", payload:response.user});
      })
    }
  }, [])
    return <nav className="navbar navbar-expand-lg navbar-light fixed-top">
    <div className="container">
      <Link className="navbar-brand" to={"/"}>Home</Link>
      {
        auth ? 
          <div>
              <div className="navbar-collapse" style={{"justifyContent": "end"}} id="navbarTogglerDemo02">
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <div onClick={()=> {
                      Cookies.remove("molo17")
                      dispatch({type:"UPDATE_USER", payload:{}});
                    }}>
                      <Icon icon="bi:person-circle" />
                      <span style={{padding:"10px"}}>Logout</span>
                    </div>
                 </li>
                 </ul>
                </div>
          </div>
        :
          <NotAuth/>
      }
    </div>
  </nav>
}

export default Header