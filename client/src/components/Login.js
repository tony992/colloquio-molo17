import { useContext, useState } from "react"
import {  useNavigate } from "react-router-dom";
import { login } from "../api/autApi"
import { AppContext } from "../contexts/appContext"
import "./form.css"
const Login = ()=> {
    const navigate = useNavigate();
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const {dispatch, state} = useContext(AppContext);
    // if(state.user && state.user.email){
    //     console.log("SONOQ UJILL")
    //     navigate("/");
    // }
    const submit=(e)=> {
        e.preventDefault();
        console.log(email, password)
        return login({email,password})
        .then((response)=> {
            console.log(response)
            if(response && response.user){
                console.log(response.user)
                dispatch({type:"UPDATE_USER", payload:response.user});
                console.log(state)
                alert("login succeded")
                navigate("/");
            } else {
                alert(response.message)
            }
        })
        .catch((e)=> {
            console.log(e);



        })
    }
    return (
    <div className="auth-wrapper">
        <div className="auth-inner">
            <form onSubmit={submit}>
                <h3>Sign In</h3>
                <div className="form-group">
                    <label>Email address</label>
                    <input 
                        type="email" 
                        value={email} 
                        onChange={(e)=> {
                            setEmail(e.target.value)
                        }}
                        className="form-control" 
                        placeholder="Enter email" 
                    />
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input 
                        type="password"
                        vlaue={password}
                        onChange={(e)=> {
                            setPassword(e.target.value)
                        }}
                        className="form-control" 
                        placeholder="Enter password" 
                    />
                </div>
                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary btn-block">Submit</button>
            </form>
        </div>
    </div>
)
}

export default Login