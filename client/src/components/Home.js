import { useContext, useEffect, useMemo } from "react"
import { AppContext } from "../contexts/appContext";
import { SocketContext } from "../contexts/socketContext";
import moment from 'moment';
import Table from "./common/Table";
import { getMatch } from "../api/matchApi";


 const Home = ()=> {
    const {state,dispatch} = useContext(AppContext);
    const socket = useContext(SocketContext);

    useEffect(()=> {
        getMatch()
        .then((e)=>{
            console.log(e)
            dispatch({type:"UPDATE_MATCH", payload: e.matches});
        } )
    },[dispatch])

    const columns = useMemo(()=> {

       return([
            {
                Header: "info",
                columns: [{
                    Header: "status",
                    accessor: "status"
                },
                {
                    Header: "date",
                    accessor: (d)=> moment(d.date).format('DD/MM/YYYY')
                },
            ]            },
            {
                Header: "red team",
                columns: [{
                    Header: "striker",
                    accessor: (d)=> d.redTeam.striker.username
                },
                {
                    Header: "defensor",
                    accessor: (d)=> d.redTeam.defensor.username
                },
            ]
            },
            {
                Header: "blue Team",
                columns: [{
                    Header: "striker ",
                    accessor: (d)=> d.blueTeam.striker.username
                },
                {
                    Header: "defensor ",
                    accessor: (d)=> d.blueTeam.defensor.username
                },
            ]
            },
            {
                Header: "Score",
                columns: [{
                    Header: "blue score",
                    accessor: (d)=> d.blueScore
                },
                {
                    Header: "red score",
                    accessor: (d)=> d.redScore
                },
            ]
            },
        ])
    },[])
    
    useEffect(()=> {
        socket.once('addMatch',(match)=> {
            const matches = [].concat(...state.matches);
            const index=matches.findIndex((el)=> el._id === match);
            if(index === -1){
                matches.push(match);
    
                }
                return dispatch({type:"UPDATE_MATCH", payload:matches});

        })
        socket.once('changeScore', (match,redScore,blueScore)=> {
            console.log(match,redScore,blueScore)

        const matches = [].concat(...state.matches);
        const index=matches.findIndex((el)=> el._id === match);
        if(index !== -1){
            matches[index].blueScore = blueScore;

            matches[index].redScore = redScore;
        }
        return dispatch({type:"UPDATE_MATCH", payload:matches});
     });
     socket.once('changeStatus', (match,redScore,blueScore)=> {
        console.log(match,redScore,blueScore)

        const matches = [].concat(...state.matches);
        const index=matches.findIndex((el)=> el._id === match);
        if(index !== -1){
            matches[index].status = "ended";
        }
    // matches.push(match);      
    return dispatch({type:"UPDATE_MATCH", payload:matches});
 });
    })
    
    
    return (
        <div className="auth-wrapper">
           <Table 
            columns={columns} 
            data={state.matches} 
           />
        </div>
    );
}
export default Home