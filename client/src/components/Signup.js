import { useContext, useState } from "react"
import { useNavigate } from "react-router-dom"
import { singUp } from "../api/autApi"
import { AppContext } from "../contexts/appContext"
import "./form.css"

const Signup = ()=> {
    const navigate = useNavigate();
    const [fName, setFName] = useState("");
    const [lName, setLName] = useState("");
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const {dispatch} = useContext(AppContext);
    const submit=(e)=> {
        e.preventDefault();
        return singUp({firstName: fName, lastName: lName,email, password})
        .then((response)=> {
            console.log(response)
            if(response && response.user){
                dispatch({type:"UPDATE_USER", payload:response.user});
                alert("user registered");
                navigate("/");

            } else {
                alert(response.message)
            }
        })
    }
    return (
    <div className="auth-wrapper">
        <div className="auth-inner">
            <form onSubmit={submit}>
                <h3>Sign Up</h3>
                <div className="form-group margin-bottom">
                    <label>First name</label>
                    <input
                        value={fName}
                        onChange={(e)=> {setFName(e.target.value)}}
                        type="text"
                        className="form-control" 
                        placeholder="First name" 
                    />
                </div>
                <div className="form-group margin-bottom">
                    <label>Last name</label>
                    <input 
                        value={lName}
                        onChange={(e)=> {setLName(e.target.value)}}
                        type="text" 
                        className="form-control" 
                        placeholder="Last name" 
                        />
                </div>
                {/* <div className="form-group margin-bottom">
                    <label>Default role</label>
                    <select 
                        class="form-control" 
                        id="role"
                        value={defaultRole}
                        onChange={(e)=> {
                            console.log(e.target.value)
                            setDefaultRole(e.target.value)
                        }}
                    >
                        <option>striker</option>
                        <option>defender</option>
                       
                    </select>
                </div> */}
                <div className="form-group margin-bottom">
                    <label>Email address</label>
                    <input  
                        value={email}
                        onChange={(e)=> {setEmail(e.target.value)}}
                        type="email" 
                        className="form-control" 
                        placeholder="Enter email" 
                    />
                </div>
                <div className="form-group margin-bottom">
                    <label>Password</label>
                    <input 
                        value={password}
                        onChange={(e)=> {setPassword(e.target.value)}}
                        type="password" 
                        className="form-control" 
                        placeholder="Enter password" 
                    />
                </div>
                <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
                <p className="forgot-password text-right">
                    Already registered <a href="/sign-in">sign in?</a>
                </p>
            </form>
        </div>
    </div>)
}

export default Signup