import { AppProvider } from './contexts/appContext';
import { Routes, Route } from "react-router-dom"
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Login from './components/Login';
import Home from './components/Home';
import Register from './components/Signup';

import Header from './components/common/Header';

function App() {
  console.log(process.env)
  return (
    <div className="App">
      <AppProvider> 
        <Header>

        </Header>
        <Routes>
        
        <Route path="/" element={ <Home/> } />
        <Route path="sign-in" element={ <Login/> } />
        <Route path="sign-up" element={ <Register/> } />
      </Routes>
      </AppProvider>
    </div>
  );
}

export default App;
