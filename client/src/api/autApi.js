import config from "../config.json";

export const login=(body)=> {
    return fetch(`${config.apiUrl}/auth/login`,{
        method:"post",
        mode: "cors",  
        headers: {
            "Access-Control-Allow-Origin": `${config.apiUrl}`,
            "Access-Control-Allow-Credentials": true,
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          credentials: "include",
        body:JSON.stringify(body)
    })
    .then((response)=> {
        if(response.status === 401){
            return ({message: "not authorized email or password not valid"})
        }
        return response.json()
    })

}
console.log(process.env);
export const singUp=(body)=> {
    return fetch(`${config.apiUrl}/auth`,{
        method:"post",
        mode: "cors",  
        headers: {
            "Access-Control-Allow-Origin": `${config.clientUrl}`,
            "Access-Control-Allow-Credentials": true,
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          credentials: "include",
        body:JSON.stringify(body)
    })
    .then((response)=> {
        return response.json()
    })

}

export const getUser=(token)=> {
    return fetch(`${config.apiUrl}/auth`,{
        method:"get",
        mode: "cors",  
        headers: {
            "Access-Control-Allow-Origin": `${config.clientUrl}`,
            "Access-Control-Allow-Credentials": true,
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
          },
          credentials: "include",
    })
    .then((response)=> {
        return response.json()
    })

}