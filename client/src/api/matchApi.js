import config from '../config.json';
export const getMatch =()=> {
    return fetch(`${config.apiUrl}/match`,
        {method:"get"}
        )
        .then((el)=> {
            return el.json()
        })
}
